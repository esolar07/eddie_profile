		<?php $section='contact'; ?>
		
		<?php include ('header_e.php') ; ?>
		
		<div id="contact">
			
			<div class="connect">
				<a href="http://www.linkedin.com/profile/view?id=261969759&trk=nav_responsive_tab_profile" target="_blank"> <img src="http://brinspa.com/wp-content/themes/brin-twentyten/images/icon-linkedin-circle.png" alt="LinkedIn Eddie"/></a>
			</div>
			
			<div class="connect">
				<a href="https://www.facebook.com/eddie.solar.3" target="_blank"> <img src="http://f.cl.ly/items/1A3P0o1u221c0Z1S3W0q/facebook.png" alt="Facebook Eddie"/></a>
			</div>
			
			<div class="connect">
				<a href="mailto:esolar07@gmail.com"><img src="http://f.cl.ly/items/1t413F2p0a0A1R3F1a0u/email.png" alt="Email Me"/></a>
			</div>
		</div>

		<div id="form">
			<form>
				<p>
					Name:<input type="text" id="name" name="ep_name" class="required">
					<span>Please enter your name</span>
				</p>

				<p>
					Email:<input type="text" id="email" name="ep_email" class="required">
					<span>Please enter a valid email address</span>
				</p>

				<p>
					<textarea name="message" id="message" class="required"></textarea>
					<span>Please enter your message</span>
				</p>
				<p class="submit">
					<input type="submit" value"Submit">
				</p>
			</form>
		</div>
		<script type="text/javascript" src="JS/jquery.js"></script>
		<script type="text/javascript" src="JS/validEmail.js"></script>
		<script type="text/javascript">
			var $submit = $('.submit input');
			var $required = $('.required');
			
			function containsBlank(){
				var blanks = new Array();
				$required.each(function(){
					blanks.push($(this).val() == "");
				});
				// for older versions of ie return blanks.sort().pop();
				return $.inArray(true, blanks) !== -1;
			}

			function requiredFillIn(){
				if (containsBlank() || !$('#email').validEmail()) 
					$submit.attr("disabled", "disabled");
				else 
					$submit.removeAttr("disabled");
			}

			$('#form span').hide();
			$('input, textarea').focus(function(){
				$(this).next().fadeIn("slow");
			}).blur(function(){
				$(this).next().fadeOut("slow");
			}).keyup(function(){
				//check required fields
				requiredFillIn();
			});

			$("#email").validEmail({on:'keyup', success:function(){
				$(this).next().removeClass("error").addClass('valid');
			}, failure: function(){
				$(this).next().removeClass("valid").addClass('error');
			}});

			requiredFillIn();
		</script>
	<?php include "footer.php"; ?>