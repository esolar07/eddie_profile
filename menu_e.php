	<script type="text/javascript">
		$(init_m);

		function init_m(){
			$('#menudrop').hide();
			$('#menubarimage').click(showMenu);
		} // end init_m

		function showMenu(){
			$("#menudrop").slideToggle('slow');
		} //end showMenu
	</script>
<div class="container">
<img id="menubarimage" src="menu_image.png" style="max-width:100% height:auto;" class="img-responsive" alt="Menu Tab" >
</div>

<div class="masthead" id="menudrop">
    <ul class="nav nav-justified">
			<li class="nav" <?php if ($section == "home"){ echo "id='on'";} ?>><a href="index.php">HOME</a></li>
			<li class="nav" <?php if ($section == "aboutme"){ echo "id='on'";} ?>><a href="aboutme.php">ABOUT ME</a></li>
			<li class="nav" <?php if ($section == "resume"){ echo "id='on'";} ?>><a href="resume.php">RESUME</a></li>
			<li class="nav" <?php if ($section == "projects"){ echo "id='on'";} ?>><a href="projects.php">PROJECTS</a></li>
			<li class="nav" <?php if ($section == "contact"){ echo "id='on'";} ?>><a href="contact.php">CONTACT</a></li>
	</ul>
</div>

