<body onload="resumealert()">
  <script>
  		 		
   		$(document).ready(function(){
   			$("#about_me").click(function(){
   				$("#center").slideToggle("slow");
   			});   		
   			$("#work").click(function(){
   				$("#work2").slideToggle("slow");
   			});
   			$("#education").click(function(){
   				$("#education2").slideToggle("slow");
   			});
   		});
  </script>

  <script type="text/javascript">
    function resumealert(){
      alert ("Hello!\nPlease click on tabs to view resume details.");
    }
  </script>
   
      <?php $section='resume'; ?>
   		
  		<?php include ('header_e.php') ; ?>
		
  <div id="res">
  <div id="header">
  	   <p>Homestead FL 33034</p>
  	   <p>Eddie.Solar@live.com<p>
  	   <h1>EDDIE SOLAR</h1>
  </div>
  
  <div id="about_me">
  	<h3>Profile:</h3>
  </div>
  	
  <div id="center">
  	   	
  	   <p>Well versed and experienced in various functions and disciplines which help to successfully inform well rounded judgment and critical thinking skills. Over five years of experience in call center environment ranging from entry level, to Senior Support staff.</p>
  	   
  	   		<table id="table">
  			   <tr>
  		 	   	   <th>Managerial:</th>
			   	   <th>Technical:</th>
			   	   <th>Software:</th>
			    </tr>
		 
		 		<tr>
		 	 		<td>Team Supervision</td>
			 		<td>Policies and Procedures Manuals</td>
			 		<td>Microsoft Office Suite</td>
				</tr>
		 
		 		<tr>
		 	 		<td>Staff Development and Training</td>
			 		<td>Report and Documentation Preparation</td>
			 		<td>Visio</td>
		 		</tr>
		 
		 		<tr>
		 	 		<td> </td>
			 		<td>Spreadsheet Creation/Analytics</td>
			 		<td> </td>
				</tr>
		
				<tr>
					<td> </td>
					<td>Technical Computer Affluence</td>
					<td> </td>
				</tr>
  			</table>
  </div>
    
  <div id="work">
  	   <h3>Work Experience:</h3>
  </div>
  
  <div id="work2">
  	   <p>AnswerPro, LLC.</p>
  	   <p>Claims Specialist / Analyst</p>
  	   <p>November 2011 to Present</p>
  
  	   <ul>
  	   	   <li>Assemble, organize, and communicate customer membership/billing information necessary for Legal escalations</li>
	  	   <li>Analyze billing details in transaction report to spot possible fraud and prevent chargeback to company</li>
	  	   <li>Receive and investigate customer billing issues for resolution per guidelines and procedures</li>
	  	   <li>Communicate with Accounts Payable department to coordinate complex customer reimbursements</li>
  	   </ul>
  
  	   <p>Epic Digital Media</p>
  	   <p>Sales and Marketing Manager - Huge.com</p>
  	   <p>January 2011 to November 2011</p>
  
  	   <ul>
  	   	   <li>Created contacts and built relationships with merchants in order to inform sales leads</li>
	  	   <li>Marketed the specialized demographic of website</li>
	  	   <li>Worked with merchant to design personalized web campaign, including graphic design and text</li>
	  	   <li>Negotiated compensation percentages with merchant</li>
	  	   <li>Personally oversaw and kept regular communication with merchant throughout process until completion</li>
  	   </ul>
  
  	   <p>AnswerPro, LLC.</p>
  	   <p>Senior Trainer / Quality Assurance Analyst</p>
  	   <p>June 2008 to December 2011</p>
  
  	   <ul>
  	   	   <li>Delivered training to new hires as well as refresher training for tenured agents</li>
	  	   <li>Developed/updated training manuals and job aid documents to communicate updates to team</li>
	  	   <li>Completed special projects and ad hoc requests presented by management</li>
	  	   <li>Served as interim Training and Quality Manager while direct report was on leave</li>
	  	   <li>Served as Naked Program subject matter expert (SME)</li>
  	   </ul>
  
  	   <p>AnswerPro, LLC.</p>
  	   <p>Fraud Analyst - Part-Time</p>
  	   <p>May 2008 to July 2008</p>
  
  	   <ul>
  	   	   <li>Research and analyze account activity to assess levels of risk</li>
	  	   <li>Took appropriate actions based on indicators of complex issues related to possible identity theft</li>
	  	   <li>Made decisions that would directly affect the customer user experience based on risk to the company</li>
  	   </ul>
  
  	   <p>AnswerPro, LLC.</p>
  	   <p>Customer Service Representative</p>
  	   <p>May 2007 to April 2008</p>
  
  	   <ul>
  	   	   <li>Supported several programs of varying nature including: Fling.com, Bangbros.com, RealityKings, etc.</li>
	 	   <li>Served as dedicated Billing Support Specialist, receiving and closing inbound sales opportunities</li>
  	   </ul>
  </div>
  
  <div id="education">
  	   <h3>Education:</h3>
  </div>
  
  <div id="education2">
  	   <p>Miami Senior High School </p>
  	   <p>1997 to 2001</p>
  	   <p>High School Diploma </p>
  </div>
  </div>
  
  <?php include "footer.php"; ?>
</body>